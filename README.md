[![pipeline status](https://gitlab.com/camiloahm/mysql-k8s/badges/master/pipeline.svg)](https://gitlab.com/camiloahm/soap-ui-tests/commits/master)

# MySQL + Gitlab

Gilab job to create a MySql with a dummy db

This Small project shows how to use interesting things from gitlab like anchors and tags. 

From K8s It shows how to create a mysql server but it also populates with a DB during the start up (Seeding a Database)

Also if you take look at ``db-deployment.yml`` you can see how is the deployment delayed until the Dependencies are ready 

If you want to check your db after the deployment ends, you can use this 

kubectl run -it --rm --image=mysql --restart=Never mysql-client -- mysql -h mysql-service -uroot -p<YOUR_PASSWORD>

