#!/bin/bash
set -x
set -e

sed -i -e "s/{NAMESPACE}/${NAMESPACE}/g" k8s/namespace.yml
sed -i -e "s/{NAMESPACE}/${NAMESPACE}/g" k8s/configmap.yml
sed -i -e "s/{NAMESPACE}/${NAMESPACE}/g" k8s/secret.yml
sed -i -e "s/{NAMESPACE}/${NAMESPACE}/g" k8s/db-deployment.yml
sed -i -e "s/{NAMESPACE}/${NAMESPACE}/g" k8s/db-pv.yml
sed -i -e "s/{NAMESPACE}/${NAMESPACE}/g" k8s/db-service.yml

kubectl apply -f k8s/namespace.yml
kubectl delete --ignore-not-found --grace-period=0 --force -n "${NAMESPACE}" -f k8s/db-deployment.yml
kubectl delete --ignore-not-found --grace-period=0 --force -n "${NAMESPACE}" -f k8s/db-pv.yml
kubectl delete configmap configmap-initdb --ignore-not-found -n "${NAMESPACE}"
kubectl create configmap configmap-initdb -n "${NAMESPACE}" --from-file="k8s/sql.sql"
kubectl apply -n "${NAMESPACE}" -f k8s/configmap.yml
kubectl apply -n "${NAMESPACE}" -f k8s/secret.yml
kubectl apply -n "${NAMESPACE}" -f k8s/db-pv.yml
kubectl apply -n "${NAMESPACE}" -f k8s/db-deployment.yml
kubectl apply -n "${NAMESPACE}" -f k8s/db-service.yml
